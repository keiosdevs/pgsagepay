<?php namespace Keios\PGSagePay;

use Illuminate\Foundation\AliasLoader;
use Keios\PGSagePay\Classes\SagePayChargeMaker;
use Keios\PGSagePay\Operators\SagePay;
use System\Classes\PluginBase;
use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use Keios\PaymentGateway\Models\Settings;
use Event;

/**
 * PgSagePay Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array
     */
    public $require = [
        'Keios.PaymentGateway',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'keios.pgsagepay::lang.plugin.name',
            'description' => 'keios.pgsagepay::lang.plugin.description',
            'author'      => 'Keios',
            'icon'        => 'icon-money',
        ];
    }

    /**
     * Plugin register method
     */
    public function register()
    {

        Settings::extend(
            function (Settings $settings) {
                $settings->addDynamicMethod(
                    'getSagepay.envOptions',
                    function () {
                        return [
                            'test' => trans('keios.pgsagepay::lang.settings.test'),
                            'live' => trans('keios.pgsagepay::lang.settings.live'),
                        ];
                    }
                );
            }
        );

        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGSagePay\Operators\SagePay');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'sagepay.general'              => [
                            'label' => 'keios.pgsagepay::lang.settings.general',
                            'tab'   => 'keios.pgsagepay::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'sagepay.info'                 => [
                            'type' => 'partial',
                            'path' => '@/plugins/keios/pgsagepay/partials/_sagepay_info.htm',
                            'tab'  => 'keios.pgsagepay::lang.settings.tab',
                        ],
                        'sagepay.vendorName'           => [
                            'label'   => 'keios.pgsagepay::lang.settings.vendorName',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'sagepay.formPasswordTest'     => [
                            'label'   => 'keios.pgsagepay::lang.settings.formPasswordTest',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'span'    => 'left',
                            'default' => '',
                        ],
                        'sagepay.formPasswordLive'     => [
                            'label'   => 'keios.pgsagepay::lang.settings.formPasswordLive',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'span'    => 'right',
                            'default' => '',
                        ],

                        'sagepay.siteFqdnsTest'        => [
                            'label'   => 'keios.pgsagepay::lang.settings.siteFqdnsTest',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'span'    => 'left',
                            'default' => '',
                        ],
                        'sagepay.siteFqdnsLive'        => [
                            'label'   => 'keios.pgsagepay::lang.settings.siteFqdnsLive',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'span'    => 'right',
                            'default' => '',
                        ],
                        'sagepay.env'                  => [
                            'label'   => 'keios.pgsagepay::lang.settings.mode',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'dropdown',
                            'default' => 'test',
                        ],
                        'sagepay.customerPasswordSalt' => [
                            'label'   => 'keios.pgsagepay::lang.settings.customerPasswordSalt',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '!!!CHANGEIT!!!',
                        ],
                        // advanced
                        'paypal.advanced'              => [
                            'label' => 'keios.pgsagepay::lang.settings.advanced',
                            'tab'   => 'keios.pgsagepay::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'sagepay.requestTimeout'       => [
                            'label'   => 'keios.pgsagepay::lang.settings.requestTimeout',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '30',
                        ],
                        'sagepay.applyAvsCv2'          => [
                            'label'   => 'keios.pgsagepay::lang.settings.applyAvsCv2',
                            'comment' => 'keios.pgsagepay::lang.settings.applyAvsCv2Desc',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'span'    => 'left',
                            'default' => '0',
                        ],
                        'sagepay.apply3dSecure'        => [
                            'label'   => 'keios.pgsagepay::lang.settings.apply3dSecure',
                            'comment' => 'keios.pgsagepay::lang.settings.apply3dSecureDesc',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'span'    => 'right',
                            'default' => '0',
                        ],
                        'sagepay.vendorData'           => [
                            'label'   => 'keios.pgsagepay::lang.settings.vendorData',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'sagepay.PartnerId'            => [
                            'label'   => 'keios.pgsagepay::lang.settings.PartnerId',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'type'    => 'text',
                            'span'    => 'left',
                            'default' => '',
                        ],
                        'sagepay.allowGiftAid'         => [
                            'label'   => 'keios.pgsagepay::lang.settings.allowGiftAid',
                            'comment' => 'keios.pgsagepay::lang.settings.allowGiftAidDesc',
                            'tab'     => 'keios.pgsagepay::lang.settings.tab',
                            'span'    => 'right',
                            'type'    => 'switch',
                            'default' => false,
                        ],

                    ]
                );
            }
        );
    }

}
