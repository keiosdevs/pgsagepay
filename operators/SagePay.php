<?php namespace Keios\PGSagePay\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Models\Settings;
use Keios\PaymentGateway\Support\HashIdsHelper;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PGSagePay\Classes\SagePayConfiguration;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PGSagePay\Classes\SagePayChargeMaker;
use Keios\PaymentGateway\Core\Operator;


/**
 * Class SagePay
 *
 * @package Keios\PGSagePay
 */
class SagePay extends Operator implements StatefulInterface
{
    use SettingsDependent;

    const CREDIT_CARD_REQUIRED = true;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgsagepay::lang.operators.sagepay';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgsagepay/assets/img/sagepay/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {

        $chargeMaker = new SagePayChargeMaker($this->cart, $this->paymentDetails, $this->creditCard);
        $charge = $chargeMaker->make();

        dd($charge);

    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        if ($this->isPaidInSagePay && $this->can(Operator::TRANSITION_ACCEPT)) {
            try {
                $this->accept();
            } catch (\Exception $ex) {
                \Log::error($ex->getMessage());
            }

            return \Redirect::to($this->returnUrl);
        } else {
            return \Redirect::to($this->returnUrl);
        }
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {

    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function extractUuid(array $data)
    {

    }

    protected function setApiKey()
    {

    }

    protected function makeCardErrorMessage()
    {

    }
}
