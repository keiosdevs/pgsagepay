<?php

return [
    'plugin'    => [
        'name'        => 'PgSagePay',
        'description' => 'No description provided yet...',
    ],
    'operators' => [
        'sagepay' => 'SagePay',
    ],
    'info' => [
        'header' => 'How to retrieve your SagePay data'
    ],
    'settings'  => [
        'test'                 => 'test',
        'live'                 => 'live',
        'general'              => 'General',
        'tab'                  => 'SagePay',
        'vendorName'           => 'Vendor name',
        'formPasswordTest'     => 'Vendor password (Test mode)',
        'formPasswordLive'     => 'Vendor password (Live mode)',
        'siteFqdnsTest'        => 'Website address (Test mode)',
        'siteFqdnsLive'        => 'Website address (Live mode)',
        'formSuccessUrl'       => 'Page to which redirect on success',
        'formFailureUrl'       => 'Page to which redirect on failure',
        'mode'                 => 'Mode',
        'accountType'          => 'Account Type (usually E)',
        'advanced'             => 'Advanced Settings',
        'txType'               => 'TxType (usually PAYMENT)',
        'PartnerId'            => 'Partner ID (optional)',
        'requestTimeout'       => 'Request Timeout (optional, usually 30)',
        'vendorData'           => 'Vendor dataa (optional)',
        'applyAvsCv2'          => 'Apply AVS/CV2',
        'applyAvsCv2Desc'      => "0 = If AVS/CV2 enabled then check them.  If rules apply, use rules (default).
1 = Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules.
2 = Force NO AVS/CV2 checks even if enabled on account.
3 = Force AVS/CV2 checks even if not enabled for the account but DONT apply any rules.",
        'apply3dSecure'        => 'keios.pgsagepay::lang.settings.apply3dSecure',
        'apply3dSecureDesc'    => "0 = If 3D-Secure checks are possible and rules allow, perform the checks and apply the authorisation rules. (default)
1 = Force 3D-Secure checks for this transaction if possible and apply rules for authorisation.
2 = Do not perform 3D-Secure checks for this transaction and always authorise.
3 = Force 3D-Secure checks for this transaction if possible but ALWAYS obtain an auth code, irrespective of rule base.",
        'allowGiftAid'         => 'Allow Gift Aid',
        'allowGiftAidDesc'     => 'For charities registered for Gift Aid, set to 1 to display the Gift Aid check',
        'customerPasswordSalt' => 'Customer Password Salt',
    ],
];