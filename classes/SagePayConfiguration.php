<?php
/**
 * Created by Keios Solutions.
 * User: Jakub Zych
 * Date: 11/17/15
 * Time: 11:27 AM
 */

namespace Keios\PGSagePay\Classes;

use Keios\PaymentGateway\Traits\SettingsDependent;


/**
 * Class SagePayConfiguration
 *
 * @package Keios\PGSagePay\Classes
 */
class SagePayConfiguration
{
    use SettingsDependent;

    /**
     * @param string $currency
     *
     * @return \SagepaySettings
     */
    public function getSetup($currency)
    {
        $this->getSettings();
        $urls = $this->prepareUrls();

        $setup = \SagepaySettings::getInstance([], false);
        $setup->setProtocolVersion(3.00);

        $setup->setEnv($this->settings->get('sagepay.env'));
        $setup->setVendorName($this->settings->get('sagepay.vendorName'));
        $setup->setPartnerId($this->settings->get('sagepay.PartnerId'));
        $setup->setVendorData($this->settings->get('sagepay.vendorData'));
        $setup->setApplyAvsCv2($this->settings->get('sagepay.applyAvsCv2'));
        $setup->setApply3dSecure($this->settings->get('sagepay.apply3dSecure'));
        $setup->setAllowGiftAid($this->settings->get('sagepay.allowGiftAid'));
        $setup->setCustomerPasswordSalt($this->settings->get('sagepay.customerPasswordSalt'));
        $setup->setCurrency($currency);

        $setup->setCollectRecipientDetails(false);
        $setup->setTxType('PAYMENT');
        $setup->setFormSuccessUrl($urls['success_url']);
        $setup->setFormFailureUrl($urls['fail_url']);
        $setup->setAccountType('E');
        $setup->setSendEmail(0);
        $setup->setBillingAgreement(1);
        $setup->setLogError(true);
        // todo. breaks things.
        //$setup->setRequestTimeout($this->settings->get('sagepay.requestTimeout'));

        $setup->setSiteFqdn($this->settings->get('sagepay.siteFqdnsTest'), 'test');
        $setup->setSiteFqdn($this->settings->get('sagepay.siteFqdnsLive'), 'live');
        $setup->setFormPassword(
            [
                'test' => $this->settings->get('sagepay.formPasswordTest'),
                'live' => $this->settings->get('sagepay.formPasswordLive'),
            ]
        );

        return $setup;
    }

    /**
     * @return array
     */
    private function prepareUrls()
    {
        $successUrl = '';
        $failUrl = '';

        return ['success_url' => $successUrl, 'fail_url' => $failUrl];
    }

}