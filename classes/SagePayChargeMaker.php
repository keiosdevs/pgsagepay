<?php namespace Keios\PGSagePay\Classes;

use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\ValueObjects\CreditCard;

/**
 * Class PayLaneChargeMaker
 *
 * @package Keios\PGPayLane\Classes
 */
class SagePayChargeMaker
{
    /**
     * @var Orderable
     */
    protected $cart;

    /**
     * @var Details
     */
    protected $details;

    /**
     * @var CreditCard
     */
    protected $card;

    /**
     * PayLaneChargeMaker constructor.
     *
     * @param Orderable  $cart
     * @param Details    $details
     * @param CreditCard $card
     */
    public function __construct(Orderable $cart, Details $details, CreditCard $card)
    {
        $this->cart = $cart;
        $this->details = $details;
        $this->card = $card;
    }


    /**
     * @return \SagepayDirectApi
     * @throws \ValidationException
     */
    public function make()
    {
        $currency = $this->cart->getTotalGrossCost()->getCurrency()->getIsoCode();
        $configuration = new SagePayConfiguration();
        $setup = $configuration->getSetup($currency);
        $basket = $this->setBasket();

        $card = [
            'cardNumber' => $this->card->getNumber(),
            'cardHolder' => $this->card->getFirstName().' '.$this->card->getLastName(),
            'expiryDate' => $this->card->getExpiryDate('Y-m'),
            'cv2'        => $this->card->getCvv(),
            'giftAid'    => 0,
        ];

        // create API request with configuration and basket
        $api = new \SagepayDirectApi($setup);
        $api->setBasket($basket);

        // validate and get token for card
        $sagepayToken = new \SagepayToken($setup);
        $token = $sagepayToken->register($card);
        if(!$token){
            throw new \ValidationException(['error' => trans('keios.pgsagepay::lang.errors.invalid_card_details')]);
        }


//        //todo or to remove - shipping is in basket afaik?
//        if ($this->cart->hasShipping()) {
//            $shipping = $this->cart->getShipping();
//            $charge['shipping'] = [
//                'name'     => $shipping->getName(),
//                'cost'     => $shipping->getGrossCost()->getAmountBasic(),
//                'currency' => $shipping->getGrossCost()->getCurrency()->getIsoCode(),
//                'tax'      => $shipping->getTax().'%',
//            ];
//        }

        return $api;
    }

    /**
     * @return \SagepayBasket
     */
    protected function setBasket()
    {
        $basket = new \SagepayBasket();

        $basket->setDescription($this->details->getDescription());
        $items = [];

        foreach ($this->cart->getAll() as $item) {
            $netAmount = $item->getSingleNetPrice()->getAmountString();
            $vatAmount = $item->getTaxValue()->getAmountString();
            $basketItem = new \SagepayItem();
            $basketItem->setUnitNetAmount($netAmount);
            $basketItem->setUnitTaxAmount($vatAmount);
            $basketItem->setQuantity($item->getCount());
            $basketItem->setDescription($item->getDescription());
            array_push($items, $basketItem);
        }


        $basket->setItems($items);

        return $basket;
    }
}